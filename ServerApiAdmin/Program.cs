﻿using Ascon.Pilot.Common;
using Ascon.Pilot.Common.DataProtection;
using Ascon.Pilot.DataClasses;
using Ascon.Pilot.Server.Api;
using Ascon.Pilot.Server.Api.Contracts;
using System;
using System.Linq;

namespace ServerApiAdmin
{
    internal class Program
    {
        static void Main(string[] args)
        {

            var credantials = ConnectionCredentials.GetConnectionCredentials("http://localhost:5545/pilot-bim_ru", "frolov", "k2yCEHTD".ConvertToSecureString());
            var client = new HttpPilotClient(credantials.GetConnectionString(), credantials.GetConnectionProxy());
            client.Connect(false);

            var authApi = client.GetAuthenticationApi();
            authApi.LoginServerAdministrator(credantials.Username, credantials.ProtectedPassword, false);
            var serverAdminApi = client.GetServerAdminApi(new ServerAdminApiCallback());
            serverAdminApi.OpenDatabase(credantials.DatabaseName);

            var admins = serverAdminApi.GetServerAdministrators();
            var admin = admins.FirstOrDefault(x => x.Login == "ServerAdmin");
            serverAdminApi.DeleteServerAdministrator(admin);

            Console.ReadKey();
            client.Disconnect();


        }
    }

    public class ServerAdminApiCallback : IServerAdminCallback
    {
        public void NotifyConnectionCountFromServer(int licenseType, int licenseCount)
        {

        }

        public void NotifyDatabaseChanged(DatabaseChangeset changeset)
        {

        }

        public void NotifyDMetadataChanged(string databaseName, DMetadataChangeset changeset)
        {

        }

        public void NotifyIndexingStateChanged(string databaseName, ushort percent)
        {

        }

        public void NotifyOrganisationUnitChanged(string databaseName, OrganisationUnitChangeset changeset)
        {

        }

        public void NotifyPersonChanged(string databaseName, PersonChangeset changeset)
        {

        }

        public void NotifyReservationsChanged()
        {

        }
    }
}
