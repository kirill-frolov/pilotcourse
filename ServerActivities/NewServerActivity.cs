﻿using Ascon.Pilot.DataModifier;
using Ascon.Pilot.ServerExtensions.SDK;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Threading.Tasks;

namespace ServerActivities
{
    [Export(typeof(IServerActivity))]
    public class NewServerActivity : IServerActivity
    {
        public string Name => "NewServerActivity";

        public Task RunAsync(IModifierBase modifier, IModifierBackend backend, 
            IServerActivityContext serverActivityContext, 
            IAutomationEventContext automationEventContext)
        {
            var types = backend.GetMetadata().Types;
            if (!serverActivityContext.Params.TryGetValue("customParam", out var value))
                return Task.CompletedTask;
            var source = automationEventContext.Source;
            var builder = modifier.EditObject(source);
            builder.SetAttribute("name", new Ascon.Pilot.DataClasses.DValue() { Value = "Name from server activity"});
            return Task.CompletedTask;
        }
    }
}
