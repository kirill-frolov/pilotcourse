﻿using Ascon.Pilot.Common;
using Ascon.Pilot.Common.DataProtection;
using Ascon.Pilot.DataClasses;
using Ascon.Pilot.DataModifier;
using Ascon.Pilot.Server.Api;
using Ascon.Pilot.Server.Api.Contracts;
using Ascon.Pilot.Transport;
using System;
using System.IO;
using System.Linq;

namespace ServerApiConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var credantials = ConnectionCredentials.GetConnectionCredentials("http://localhost:5545/pilot-bim_ru", "frolov", "k2yCEHTD".ConvertToSecureString());
            var client = new HttpPilotClient(credantials.GetConnectionString(), credantials.GetConnectionProxy());
            client.Connect(false);

            var authApi = client.GetAuthenticationApi();
            //90 - Bim
            //103 - Enterprise
            //100 - ICE
            authApi.Login(credantials.DatabaseName, credantials.Username, credantials.ProtectedPassword, false, 90);
            var serverApi = client.GetServerApi(new ServerApiCallBack());
            var messageApi = client.GetMessagesApi(new MessageApiCallBack());
            var fileArchiveApi = client.GetFileArchiveApi();
            var tmpPath = @"D:\Tmp";
            if (!Directory.Exists(tmpPath))
                Directory.CreateDirectory(tmpPath);
            var storageProvider = new FileSystemStorageProvider(tmpPath);
            var changesetUploader = new ChangesetUploader(fileArchiveApi, storageProvider);
            var backend = new Backend(serverApi, messageApi, changesetUploader);
            var types = backend.GetMetadata().Types;
            var obj = backend.GetObject(new Guid("ebad3615-4c7b-4390-b649-fe93acbe343f"));
            if (obj != null)
            {
                var type = types.FirstOrDefault(x => x.Id == obj.TypeId);
                Console.WriteLine(obj.GetTitle(type));

                var modifier = new Modifier(backend);
                var newDocBuilder = modifier.CreateObject(Guid.NewGuid(), obj.ParentId, obj.TypeId);
                newDocBuilder.SetAttribute("name", "New doc from server api")
                    .SetAttribute("sheet_number", 2);
                if (modifier.AnyChanges())
                    modifier.Apply();

            }

            Console.ReadKey();
            client.Disconnect();
        }

        private class MessageApiCallBack : IMessageCallback
        {
            public void CreateNotification(DNotification notification)
            {

            }

            public void NotifyMessageCreated(NotifiableDMessage message)
            {

            }

            public void NotifyOffline(int personId)
            {

            }

            public void NotifyOnline(int personId)
            {

            }

            public void NotifyTypingMessage(Guid chatId, int personId)
            {

            }

            public void UpdateLastMessageDate(DateTime maxDate)
            {

            }
        }
        private class ServerApiCallBack : IServerCallback
        {
            public void NotifyAccessChangeset(Guid objectId)
            {

            }

            public void NotifyChangeAsyncCompleted(DChangeset changeset)
            {

            }

            public void NotifyChangeAsyncError(Guid identity, ProtoExceptionInfo exception)
            {

            }

            public void NotifyChangeset(DChangeset changeset)
            {

            }

            public void NotifyCommandResult(Guid requestId, byte[] data, ServerCommandResult result)
            {

            }

            public void NotifyCustomNotification(string name, byte[] data)
            {

            }

            public void NotifyDMetadataChangeset(DMetadataChangeset changeset)
            {

            }

            public void NotifyDNotificationChangeset(DNotificationChangeset changeset)
            {

            }

            public void NotifyGeometrySearchResult(DGeometrySearchResult searchResult)
            {

            }

            public void NotifyOrganisationUnitChangeset(OrganisationUnitChangeset changeset)
            {

            }

            public void NotifyPersonChangeset(PersonChangeset changeset)
            {

            }

            public void NotifySearchResult(DSearchResult searchResult)
            {

            }
        }
    }
}
