﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Automation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientActivities
{
    [Export(typeof(IAutomationActivity))]
    public class NewClientActivity : AutomationActivity
    {
        private IObjectsRepository _objectsRepository;
        private IObjectModifier _objectModifier;

        [ImportingConstructor]
        public NewClientActivity(IObjectsRepository objectsRepository, IObjectModifier objectModifier)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
        }

        public override string Name => "NewActivity";

        public override Task RunAsync(IObjectModifier modifier, IAutomationBackend backend, IAutomationEventContext context, TriggerType triggerType)
        {
            if (!Params.TryGetValue("customParam", out var value))
                return Task.CompletedTask;
            var paramValue = value?.ToString();
            var person = context.InitiatingPerson;
            if ( person.IsAdmin)
            {
                modifier.EditById(context.Source.Id).SetAttribute("code", paramValue);
            }
            return Task.CompletedTask;
        }
    }
}
