﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace DataManagment
{
    [Export(typeof(INewTabPage))]
    internal class NewTab : INewTabPage
    {
        private ITabServiceProvider _tabServiceProvider;

        private const string BUTTON_NAME = "ButtonName";

        [ImportingConstructor]
        public NewTab(ITabServiceProvider tabServiceProvider)
        {
            _tabServiceProvider = tabServiceProvider;
        }

        public void BuildNewTabPage(INewTabPageHost host)
        {
            var groupId = Guid.NewGuid();
            host.SetGroup("Test Group", groupId);
            host.AddButtonToGroup("ButtonTitle1", BUTTON_NAME, "ButtonToolTip1", null, groupId);
            host.AddButtonToGroup("ButtonTitle2", BUTTON_NAME, "ButtonToolTip2", null, groupId);
            host.AddButtonToGroup("ButtonTitle3", BUTTON_NAME, "ButtonToolTip3", null, groupId);
        }

        public void OnButtonClick(string name)
        {
            if (name != BUTTON_NAME)
                return;
        }
    }
}
