﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Extensions;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK.Toolbar;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;


namespace DataManagment
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    [Export(typeof(IToolbar<ObjectsViewContext>))]
    [Export(typeof(IMenu<TasksViewContext2>))]
    [Export(typeof(IMenu<StorageContext>))]
    internal class Menu : ToolbarItemSubmenuHandler, IMenu<ObjectsViewContext>,
        IMenu<TasksViewContext2>,
        IMenu<StorageContext>,
        IToolbar<ObjectsViewContext>,
        IObserver<KeyValuePair<string, string>>
    {
        private IObjectsRepository _objectsRepository;
        private IObjectModifier _objectModifier;
        private IFileProvider _fileProvider;
        private IPersonalSettings _personalSettings;
        private string _settings;
        private const string MENU_NAME = "mnuExample";

        [ImportingConstructor]
        public Menu(IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IFileProvider fileProvider,
            IPersonalSettings personalSettings)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _fileProvider = fileProvider;
            _personalSettings = personalSettings;
            _personalSettings.SubscribeSetting(Consts.SETTINGS_KEY).Subscribe(this);
        }

        public void OnNext(KeyValuePair<string, string> value)
        {
            _settings = value.Value;
        }

        public void OnError(Exception error)
        { }

        public void OnCompleted()
        { }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            if (context.SelectedObjects.Count() != 1)
                return;
            var subMenuBuilder = builder.AddItem(MENU_NAME, builder.Count).WithHeader("Пример").WithSubmenu();
            subMenuBuilder.AddItem("Menuitem1", 0).WithHeader("1");
            subMenuBuilder.AddItem("Menuitem2", 1).WithHeader("2");
        }

        public void Build(IToolbarBuilder builder, ObjectsViewContext context)
        {
            if (context.SelectedObjects.Count() != 1)
                return;
            builder.AddMenuButtonItem(MENU_NAME, builder.Count)
               .WithMenu(this)
               .WithHeader("Пример");

        }

        public override void OnSubmenuRequested(IToolbarBuilder builder)
        {
            builder.AddButtonItem("Menuitem1", 0).WithHeader("Menu item 1");
            builder.AddButtonItem("Menuitem1", 1).WithHeader("Menu item 2");
        }
        public void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            OnMenuClick(name, context);

        }

        public void OnToolbarItemClick(string name, ObjectsViewContext context)
        {
            OnMenuClick(name, context);
        }

        private void OnMenuClick(string name, ObjectsViewContext context)
        {
            if (_settings is null)
                return;
            if (_settings.Equals("1"))
                MessageBox.Show("We have settings");
            if (_settings.Equals("2"))
                MessageBox.Show("We have wrong settings");
        }

        public void Build(IMenuBuilder builder, TasksViewContext2 context)
        {
            if (context.SelectedTasks.Count() != 1)
                return;
            builder.AddItem(MENU_NAME, builder.Count).WithHeader("Пример заданий");
        }

        public void OnMenuItemClick(string name, TasksViewContext2 context)
        {

        }

        public void Build(IMenuBuilder builder, StorageContext context)
        {
            if (context.SelectedObjects.Count() != 1)
                return;
            builder.AddItem(MENU_NAME, builder.Count).WithHeader("Пример Storage");
        }

        public void OnMenuItemClick(string name, StorageContext context)
        {

        }


    }
}
