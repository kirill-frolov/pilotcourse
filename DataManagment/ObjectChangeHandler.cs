﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataManagment
{
    //[Export(typeof(IObjectChangeHandler))]
    internal class ObjectChangeHandler : IObjectChangeHandler
    {
        private IObjectsRepository _objectsRepository;
        private IObjectModifier _objectModifier;
        private Guid _projectId;

        [ImportingConstructor]
        public ObjectChangeHandler(IObjectsRepository objectsRepository, IObjectModifier objectModifier)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
        }


        public void OnChanged(IEnumerable<DataObjectChange> changes)
        {
            var mainSetType = _objectsRepository.GetType("main_set");
            foreach (DataObjectChange change in changes)
            {
                if (change.Old != null && change.Old.Type.Name == "project")
                {
                    if (change.Old.Id == _projectId)
                    {
                        _projectId = default;
                        continue;
                    }
                    _projectId = change.Old.Id;
                    if (change.New.Attributes.ContainsKey("code"))
                    {
                        var builder = _objectModifier.Create(change.New.Id, mainSetType);
                        builder.SetAttribute("name", "Main Set Name");
                        builder.SetAttribute("code", "Main Set Code");
                        _objectModifier.Apply();
                    }
                    
                }
            }
        }
    }
}
