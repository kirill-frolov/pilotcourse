﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagment
{
    internal class DocumentsExplorerViewModel : INotifyPropertyChanged
    {
        private ObjectCardControl _objectCardControl;
        private IObjectModifier _objectModifier;
        private Guid _id;

        public event PropertyChangedEventHandler PropertyChanged;

        public DocumentsExplorerViewModel(ObjectCardControl cardControl, IObjectModifier objectModifier, Guid id)
        {
            _objectCardControl = cardControl;
            _objectModifier = objectModifier;
            _id = id;
        }


        public IObjectModifier ObjectModifier => _objectModifier;

        public Guid Id => _id;

        public ObjectCardControl ObjectCardControl
        {
            get => _objectCardControl;
            set
            {
                _objectCardControl = value;
                OnPropertyChanged(nameof(ObjectCardControl));
            }
        }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
