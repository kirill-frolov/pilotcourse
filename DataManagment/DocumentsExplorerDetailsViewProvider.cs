﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;

namespace DataManagment
{
    [Export(typeof(IDocumentsExplorerDetailsViewProvider))]
    internal class DocumentsExplorerDetailsViewProvider : IDocumentsExplorerDetailsViewProvider
    {
        private IObjectsRepository _objectsRepository;
        private IObjectModifier _objectModifier;
        private IObjectCardControlProvider _objectCardControlProvider;

        [ImportingConstructor]
        public DocumentsExplorerDetailsViewProvider(IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IObjectCardControlProvider objectCardControlProvider)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _objectCardControlProvider = objectCardControlProvider;
        }
        public List<IType> Types => _objectsRepository.GetTypes().Where(x=>!x.HasFiles).ToList();

        public FrameworkElement GetDetailsView(ObjectsViewContext context)
        {
            var obj = context.SelectedObjects.First();

            var cardControl = _objectCardControlProvider.GetObjectCardControl(obj.Id, false);
            var viewModel = new DocumentsExplorerViewModel(cardControl, _objectModifier, obj.Id);
            return new DocumentsExplorerView { DataContext = viewModel };
        }
    }
}
