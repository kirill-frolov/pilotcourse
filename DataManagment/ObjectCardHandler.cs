﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagment
{
    //[Export(typeof(IObjectCardHandler))]
    internal class ObjectCardHandler : IObjectCardHandler
    {
        private IObjectsRepository _objectsRepository;

        [ImportingConstructor]
        public ObjectCardHandler(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
        }

        public bool Handle(IAttributeModifier modifier, ObjectCardContext context)
        {
            return true;
        }

        public bool OnValueChanged(IAttribute sender, AttributeValueChangedEventArgs args, IAttributeModifier modifier)
        {
           var person = _objectsRepository.GetCurrentPerson();
            if (!person.IsAdmin)
                return false;
            if (sender.Name == "code")
            {

                if (args.NewValue != null)
                {
                    if (args.NewValue.ToString() == "code")
                        modifier.SetValue("project_name", "Name from plaugin");
                }
            }
            return true;
        }
    }
}
