﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataManagment
{
    /// <summary>
    /// Interaction logic for SettingsFeatureView.xaml
    /// </summary>
    public partial class SettingsFeatureView : UserControl
    {
        private ISettingValueProvider _settingValueProvider;

        public SettingsFeatureView(ISettingValueProvider settingValueProvider)
        {
            InitializeComponent();
            _settingValueProvider = settingValueProvider;
            var settings = settingValueProvider.GetValue();
            InputText.Text = settings;

        }


        private void InputText_TextChanged(object sender, TextChangedEventArgs e)
        {
            _settingValueProvider.SetValue(InputText.Text);
        }
    }
}
