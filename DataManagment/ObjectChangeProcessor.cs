﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DataManagment
{
    //[Export(typeof(IObjectChangeProcessor))]
    internal class ObjectChangeProcessor : IObjectChangeProcessor
    {
        private IPilotDialogService _pilotDialogService;

        [ImportingConstructor]
        public ObjectChangeProcessor(IPilotDialogService pilotDialogService)
        {
            _pilotDialogService = pilotDialogService;
        }

        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            _pilotDialogService.ShowBalloon("Forbidden", "Forbidden", PilotBalloonIcon.Error);
            return false;
        }
    }
}
