﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DataManagment
{
    [Export(typeof(ISettingsFeature))]
    internal class SettingsFeature : ISettingsFeature
    {
        private FrameworkElement _editor;

        public string Key => Consts.SETTINGS_KEY;

        public string Title => "Настройки для тестового модуля";

        public FrameworkElement Editor => _editor;

        public void SetValueProvider(ISettingValueProvider settingValueProvider)
        {
            _editor = new SettingsFeatureView(settingValueProvider);
        }
    }

    public static class Consts
    {
        public const string SETTINGS_KEY = "MySettings9DE60083-2C58-4177-8C87-9DF32812A6F2";
    }
}
