﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Tabs;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagment
{
    [Export(typeof(ITabsExtension<DocumentExplorerDetailsTabsContext>))]
    internal class AdditionalTab : ITabsExtension<DocumentExplorerDetailsTabsContext>
    {
        private TabView _view;
        private IObjectsRepository _objectsRepository;

        [ImportingConstructor]
        public AdditionalTab(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
        }

        public void BuildTabs(ITabsBuilder builder, DocumentExplorerDetailsTabsContext context)
        {
            _view = new TabView();
            builder.AddTab(Guid.NewGuid(), "Наша вкладка", _view);
        }

        public void OnDisposed(Guid tabId)
        {

        }

        public void OnIsActiveChanged(Guid tabId, bool isActive)
        {

        }
    }
}
