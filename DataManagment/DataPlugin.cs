﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Extensions;

namespace DataManagment
{
    //[Export(typeof(IDataPlugin))]
    public class DataPlugin : IDataPlugin, IObserver<IDataObject>, IObserver<ISearchResult>
    {
        private IObjectsRepository _objectsRepository;
        private ISearchService _searchService;

        [ImportingConstructor]
        public DataPlugin(IObjectsRepository objectsRepository,
            ISearchService searchService)
        {
            _objectsRepository = objectsRepository;
            _objectsRepository.SubscribeObjects(new[] { SystemObjectIds.RootObjectId }).Subscribe(this);
            _searchService = searchService;
            var documentType = _objectsRepository.GetType("document");
            var queryBuilder = _searchService.GetEmptyQueryBuilder();
            queryBuilder.Must(ObjectFields.TypeId.BeAnyOf(new int[] { documentType.Id }))
                .InContext(new Guid("88dfed13-f1a0-4735-b1fc-55a71743ea5b"))
                //.MustAnyOf(new[] { AttributeFields.Integer("sheet_number").Be(1), AttributeFields.Integer("sheet_number").Be(2) })
                .Must(AttributeFields.Integer("sheet_number").BeAnyOf(1, 2))
                .MaxResults(int.MaxValue);

            _searchService.Search(queryBuilder).Subscribe(this);
        }


        public async void OnNext(ISearchResult value)
        {
            if (value.Kind == SearchResultKind.Remote)
            {
                var objects = await _objectsRepository.GetObjectsAsync(value.Result, System.Threading.CancellationToken.None);

            }
        }


        public async void OnNext(IDataObject value)
        {
            var attr = value.Attributes;
            var obj = await _objectsRepository.GetObjectAsync(value.Id, System.Threading.CancellationToken.None);
        }


        public void OnCompleted()
        {

        }

        public void OnError(Exception error)
        {

        }


    }
}
