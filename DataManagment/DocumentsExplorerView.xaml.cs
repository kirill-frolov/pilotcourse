﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataManagment
{
    /// <summary>
    /// Interaction logic for DocumentsExplorerView.xaml
    /// </summary>
    public partial class DocumentsExplorerView : UserControl
    {
        public DocumentsExplorerView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (DocumentsExplorerViewModel)DataContext;
            if (!viewModel.ObjectCardControl.Interaction.GetIsValidInput())
                return;
            var values = viewModel.ObjectCardControl.Interaction.GetValues();
            var builder  = viewModel.ObjectModifier.EditById(viewModel.Id);
            foreach ( var value in values ) 
            {
                builder.SetAttributeAsObject(value.Key, value.Value.Value);
            }
            viewModel.ObjectModifier.Apply();


        }
    }
}
