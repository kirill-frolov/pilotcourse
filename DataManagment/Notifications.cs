﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagment
{
    [Export(typeof(IDataPlugin))]
    internal class Notifications : IDataPlugin, IObserver<INotification>
    {
        private IObjectsRepository _objectsRepository;

        [ImportingConstructor]
        public Notifications(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
            _objectsRepository.SubscribeNotification(NotificationKind.ObjectCreated).Subscribe(this);
            _objectsRepository.SubscribeNotification(NotificationKind.ObjectAttributeChanged).Subscribe(this);
        }

        public void OnNext(INotification value)
        {
            switch (value.ChangeKind)
            {
                case NotificationKind.ObjectCreated:
                    break;
                case NotificationKind.ObjectAttributeChanged:
                    break;
            }
        }

        public void OnCompleted()
        {

        }

        public void OnError(Exception error)
        {

        }


    }
}
